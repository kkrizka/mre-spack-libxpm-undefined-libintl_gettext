# MRE Spack libxpm Linking to libintl

Installing the built-in libxpm package fails with missing objects `libintl_*`.

```
$ spack install libxpm
...
libtool: link: /opt/rh/gcc-toolset-12/root/usr/bin/gfortran -g -O2 -o .libs/whizard -I/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/libtirpc-1.2.6-hin7vgkgxx2edogeykaidfqfjkpo4oeo/include/tirpc -Wl,-rpath -Wl,/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/hepmc3-3.2.5-yxkmuxepeit3vatzbycf6qubwbujesp5/lib64 -Wl,-rpath -Wl,/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/pythia8-8.306-dcjhzodclelvsneccjou2x36jezjdb6n/lib -Wl,-rpath -Wl,/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/pythia8-8.306-dcjhzodclelvsneccjou2x36jezjdb6n/lib  main/.libs/libwhizard_main.so -L/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/pythia8-8.306-dcjhzodclelvsneccjou2x36jezjdb6n/lib ./.libs/libwhizard.so -L/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/openloops-2.1.2-zxssflzfqsnbfr7dicorrn6e5c3bdbrd/lib /tmp/root/spack-stage/spack-stage-whizard-3.0.3-7gpuzqc2653tkxhhxhiljsxv4af2sqsx/spack-src/vamp/src/.libs/libvamp.so /tmp/root/spack-stage/spack-stage-whizard-3.0.3-7gpuzqc2653tkxhhxhiljsxv4af2sqsx/spack-src/circe1/src/.libs/libcirce1.so /tmp/root/spack-stage/spack-stage-whizard-3.0.3-7gpuzqc2653tkxhhxhiljsxv4af2sqsx/spack-src/circe2/src/.libs/libcirce2.so -lcuttools -lopenloops -loneloop -lolcommon -lrambo -ltrred prebuilt/.libs/libwhizard_prebuilt.so -L/opt/rh/gcc-toolset-12/root/usr/lib/gcc/x86_64-redhat-linux/12 -L/opt/rh/gcc-toolset-12/root/usr/lib/gcc/x86_64-redhat-linux/12/../../../../lib64 -L/lib/../lib64 -L/usr/lib/../lib64 -L/opt/rh/gcc-toolset-12/root/usr/lib/gcc/x86_64-redhat-linux/12/../../.. -lstdc++ -lm -lgcc_s -ltirpc -L/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/hepmc3-3.2.5-yxkmuxepeit3vatzbycf6qubwbujesp5/lib64 -lHepMC3 -lpythia8 -ldl -Wl,-rpath -Wl,/opt/spack/opt/spack/linux-almalinux9-x86_64/gcc-12.2.1/whizard-3.0.3-7gpuzqc2653tkxhhxhiljsxv4af2sqsx/lib
/opt/rh/gcc-toolset-12/root/usr/libexec/gcc/x86_64-redhat-linux/12/ld: cannot find -ltirpc: No such file or directory
```

This occurs with spack v0.20.0 and the latest `develop` branch (ref `935f862`
at the time of writing) of [Spack](https://github.com/spack/spack).